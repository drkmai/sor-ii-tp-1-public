#include <stdio.h>
#include <stdlib.h>
#include "readHeaders.h"
#include <stdbool.h>
#include <string.h>

unsigned short firstClustersArray[512];
unsigned short rootEntryArray[512];
char dirname[512][100];
int arrayIndex = 0;

bool print_file_info(Fat12Entry *entry) {
    switch(entry->filename[0]) {
    case 0x00:
        return false; // unused entry
    case 0xE5: // Completar los ...
        printf("Archivo borrado: [?%.7s.%.3s]\n",entry->name,entry->extension);
        return false;
    default:
        switch(entry->attributes[0]) {
            case 0x20:
                printf("Archivo: [%.1s%.7s.%.3s]\n",  entry->filename, entry->name, entry->extension);
                return false;
            case 0x10:
                if (entry->filename[0]!='.'){

                    printf("Directorio: [%.1s%.7s.%.3s]\n",  entry->filename, entry->name, entry->extension);
                    printf(" First cluster number [%i]\n",entry->cluster_lowbytes_address);
                    char dirnameStr[100];
                    strcpy(dirnameStr,entry->filename);
                    strcpy(dirname[arrayIndex], dirnameStr);
                    firstClustersArray[arrayIndex++] = entry->cluster_lowbytes_address;
                    return true;
                }
                return false;
        }
        return false;
    }
    
}

int main() {
    FILE * in = fopen("test.img", "rb");
    int i,j,k;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry,entryDir;
	fseek(in,446,SEEK_SET); // voy al principio de la tabla de particones.
    fread(pt,sizeof(PartitionTable),4,in);
    
    for(i=0; i<4; i++) {        
        if(pt[i].partition_type == 1) {
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }
    
    fseek(in, 0, SEEK_SET);
	fread(&bs,sizeof(Fat12BootSector),1,in);
    
    printf("En  0x%X, sector size %d, FAT size %d sectors, %d FATs\n\n", 
           ftell(in), bs.sector_size, bs.fat_size_sectors, bs.number_of_fats);
           
    fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
          bs.sector_size, SEEK_CUR);
    
    printf("Root dir_entries %d \n", bs.root_dir_entries);
    for(i=0; i<bs.root_dir_entries; i++) {
        fread(&entry, sizeof(entry), 1, in);
        if (print_file_info(&entry)){
            rootEntryArray[arrayIndex-1] = i;
        }
    }
    for (j = 0; j < arrayIndex;j++){
        fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
        bs.sector_size * firstClustersArray[j]* rootEntryArray[j], SEEK_SET);
        printf("Archivos del directorio %s\n",dirname[j]);
        for (k = 0; k < bs.root_dir_entries; k++){
            fread(&entry, sizeof(entry), 1, in);
            print_file_info(&entry);
        }
    }
    
    printf("\nLeido Root directory, ahora en 0x%X\n", ftell(in));
    fclose(in);
    return 0;
}

/* ESTO FUNCA
for (j = 0; j < index;j++){
        fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
        bs.sector_size * firstClustersArray[j]* rootEntryArray[j], SEEK_SET);
        for (k = 0; k < bs.root_dir_entries; k++){
            fread(&entry, sizeof(entry), 1, in);
            print_file_info(&entry);
        }
 */

/*
 fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
        bs.sector_size + rootEntryArray[j] , SEEK_SET); //* rootEntryArray[j]
        fread(&entry, sizeof(entry), 1, in);
        printf("Archivos del Directorio: [%.1s%.7s.%.3s]\n",entry.filename, entry.name, entry.extension);
        fseek(in, firstClustersArray[j], SEEK_CUR); // */
